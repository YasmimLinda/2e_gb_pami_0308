import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
titulo = "YASMOTORS";
cards = [
  {
    titulo : "KAWASAKI NINJA H2R",
    subtitulo : "Motocicleta",
    conteudo : "A Ninja H2R é impulsionada por um motor com potência de mais de 300 cv e o ponto chave para alcançar este rendimento incrível concentra-se no supercharger do motor -  cuja hélice tem capacidade de bombeamento de mais de 200 litros por segundo. Projetado “em casa”, com outras divisões do grupo Kawasaki, como a Gas Turbine & Machinery Company, Aerospace Company e a Corporate Technology Division, permitiu que o seu desenvolvimento fosse feito de modo a casar perfeitamente com as características da Ninja H2R.",
    foto : "https://mototour.com.br/fotos/noticias/ninja-h2r-a-moto-mais-rapida-potente-e-cara-do-brasil.jpg"
  },

  {
    titulo : "BMW R 1250 GS",
    subtitulo: "Motocicleta",
    conteudo: "As BMW R1200GS e R1200GS Adventure são motocicletas fabricadas em Berlim, Alemanha, pela BMW Motorrad, parte do grupo BMW. É parte da família BMW GS de motocicletas esportivas duplas. Ambas as motocicletas têm motor boxer de dois cilindros e 1.170 cc com quatro válvulas por cilindro. ",
    foto : "https://s2.glbimg.com/WCC2LMjWcrc51js7-I4cEvbRO4A=/0x0:620x430/600x0/smart/filters:gifv():strip_icc()/i.s3.glbimg.com/v1/AUTH_cf9d035bf26b4646b105bd958f32089d/internal_photos/bs/2020/B/j/EXqa4IS3G4j5kdNzgWJA/2016-07-01-070414-2015-bmw-r1200gs-08.jpg"
  },

  {
    titulo : "HARLEY-DAVIDSON FAT BOB 2022",
    subtitulo: "Motocicleta",
    conteudo: "a Fat Boy adota rodas perfuradas, substituindo as rodas totalmente fechadas de costume, e a Heritage aparece no site da marca com mais cromados, nas rodas e no garfo (antes eram pretos), com o para-brisa de acrílico voltando a ser transparente na parta inferior.",
    foto : "https://motos2022.pro.br/wp-content/uploads/2021/10/Harley-Davidson-Fat-Bob-2022.jpg"
    
  }
];

  constructor() {}

}
